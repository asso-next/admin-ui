import Vue from "vue";

import Vuetify from "vuetify/lib";
import fr from "vuetify/src/locale/fr";
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true
    },
    themes: {
      light: {
        primary: colors.lightBlue.accent2,
        secondary: colors.grey.lighten2,
        accent: colors.deepPurple.darken1,
        error: colors.red.darken2,
        info: colors.lightBlue.lighten3,
        success: colors.green.darken2,
        warning: colors.amber.darken2
      }
    }
  },
  lang: {
    locales: { fr },
    current: "fr"
  }
});
