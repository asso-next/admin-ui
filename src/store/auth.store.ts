import {
  UserAuth,
  register,
  disconnect,
  connectWithEmail
} from "@/services/authentication";
import { AuthState, RootState } from "./types";
import { ActionTree, MutationTree, GetterTree } from "vuex";

const state: AuthState = {
  user: {
    uid: ""
  }
};

const getters: GetterTree<AuthState, RootState> = {
  isLogin: state => !!state.user.uid
};

const actions: ActionTree<AuthState, RootState> = {
  async register({ commit }, userCredentials: UserAuth): Promise<void> {
    const user = await register(userCredentials);
    commit("SET_USER", user.user);
  },
  async connectWithEmail({ commit }, userCredentials: UserAuth): Promise<void> {
    const user = await connectWithEmail(userCredentials);
    commit("SET_USER", user.user);
  },
  async logout({ commit }) {
    await disconnect();
    commit("SET_USER", null);
  }
};

const mutations: MutationTree<AuthState> = {
  ["SET_USER"](state, user: firebase.User) {
    console.log(user);
    if (!user) state.user.uid = "";
    else state.user.uid = user.uid;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
