import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";

import auth from "./auth.store";

import { RootState } from "./types";

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  state: {},
  modules: {
    auth
  }
};

export default new Vuex.Store(store);
