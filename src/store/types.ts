export interface User {
  uid: string;
}

export interface AuthState {
  user: User;
}

export interface RootState {}
