import Vue from "vue";
import VueRouter from "vue-router";
import AuthPage from "../components/authentication/AuthPage.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/connexion",
    name: "connexion",
    component: AuthPage
  }
];

// import(/* webpackChunkName: "about" */ "../views/About.vue")
const router = new VueRouter({
  routes
});

export default router;
