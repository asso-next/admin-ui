import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBiEvo8ZsNez6n9ZsFADrr9IY4bZ6N_FrM",
  authDomain: "asso-next.firebaseapp.com",
  databaseURL: "https://asso-next.firebaseio.com",
  projectId: "asso-next",
  storageBucket: "asso-next.appspot.com",
  messagingSenderId: "261921106112",
  appId: "1:261921106112:web:c32a6e87d1af169c432525",
  measurementId: "G-5SNFB9JNM7"
};

const fireApp = firebase.initializeApp(firebaseConfig);

export { fireApp, firebase };
