import { fireApp, firebase } from "./app";

const fireAuth = fireApp.auth();

/* External signIn provider (Google, Facebook, Twitter) */
const googleProvider = new firebase.auth.GoogleAuthProvider();

export { googleProvider, fireAuth };
