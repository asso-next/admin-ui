export { fireApp } from "./app";
export { FirebaseError } from "firebase";

export { fireAuth, googleProvider } from "./auth";
