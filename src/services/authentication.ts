import { fireAuth, FirebaseError, googleProvider } from "./firebase/index";

interface UserAuth {
  email: string;
  password: string;
}

const registerErrorHandler = (authError: FirebaseError): Error => {
  const error = new Error();
  switch (authError.code) {
    case "auth/email-already-in-use":
      error.name = "email";
      error.message = "Cet email est déjà utilisé";
      break;
    case "auth/invalid-email":
      error.name = "email";
      error.message = "email invalide ou mal formaté";
      break;
    case "auth/weak-password":
      error.name = "password";
      error.message = "le mot de passe est trop court";
      break;
    default:
      console.error(authError);
      error.name = "unhandled";
      error.message = "impossible de vous enregistrer";
      break;
  }
  return error;
};

const connectionErrorHandler = (authError: FirebaseError): Error => {
  const error = new Error();

  switch (authError.code) {
    case "auth/invalid-email":
      error.name = "email";
      error.message = "email invalide ou mal formaté";
      break;
    case "auth/user-not-found":
      error.name = "email";
      error.message = "aucun compte associé à cet email";
      break;
    case "auth/wrong-password":
      error.name = "password";
      error.message = "mot de passe incorrecte";
      break;
    default:
      console.error(authError);
      error.name = "unhandled";
      error.message = "impossible de vous connecter";
      break;
  }
  return error;
};

const disconnect = (): Promise<void> => {
  return fireAuth.signOut();
};

const register = async (
  user: UserAuth
): Promise<firebase.auth.UserCredential> => {
  try {
    return await fireAuth.createUserWithEmailAndPassword(
      user.email,
      user.password
    );
  } catch (err) {
    throw registerErrorHandler(err);
  }
};

const connectWithEmail = async (
  user: UserAuth
): Promise<firebase.auth.UserCredential> => {
  try {
    return await fireAuth.signInWithEmailAndPassword(user.email, user.password);
  } catch (err) {
    throw connectionErrorHandler(err);
  }
};

const connectWithGoogle = async (): Promise<firebase.auth.UserCredential> => {
  try {
    return await fireAuth.signInWithPopup(googleProvider);
  } catch (err) {
    throw connectionErrorHandler(err);
  }
};

export { disconnect, register, connectWithEmail, connectWithGoogle, UserAuth };
